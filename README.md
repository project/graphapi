# Introduction

Graph API is a module to manage data structures containing graph information
and render them with internal or external engines.

# Installation

This module must be installed with Composer. See
https://www.drupal.org/node/1897420 for further information.

## GraphViz executable (optional)

In order to use the GraphViz engine, your must make sure the graphviz executable
is available.

Instructions for how to install Graphviz are at
https://www.graphviz.org/download/.

To install Graphviz on DDEV, see
https://ddev.readthedocs.io/en/stable/users/extend/customizing-images/#adding-extra-debian-packages-with-webimage_extra_packages-and-dbimage_extra_packages

Check for the path to the dot executable

  $ which dot

Configure the module:

1. Visit admin/config/system/graphapi:
2. On any of the formats using the graphviz engine, select 'Configure engine'
  from the Operations dropbutton
3. Fill in the path to the dot executable ie : /usr/local/bin/dot

## Test your configuration

1. Visit admin/config/system/graphapi
2. Select 'Demo' from the Operations dropbutton for any of the available formats

# Usage

Graphs can be created with Views, or directly in PHP code with a render array.

## Views

Create a view and select 'Graph' for its format.

Each row in the view represents an edge in the graph. There must be one field
which gives the ID of the 'from' edge and one field which gives the ID of the
'to' edge.

The vertices in the graph are added automatically based on the definition of
the edges. (This means it's not possible to have a vertex with no edges.)

If a row has an empty value for either of the two vertex ID fields, it is
skipped. A warning is showed for this, which can be turned off.

If a row has multiple values for either of the two vertex ID fields, only the
first value is taken. A warning is showed for this, which can be turned off. To
deal with multi-valued entity reference fields, add the reference relationship
and use the entity ID field on the relationship for the vertex ID. Making the
relationship required will also prevent rows with empty ID values.

Two fields can also be selected for the labels of the vertices. If these are not
selected, the vertex IDs are used as labels. If a vertex appears in multiple
rows and these rows have different values for the label field, the value for the
first row is used for the label.

## Render array

Assemble a graph as a \Fhaculty\Graph\Graph object:

```
$graph = new \Fhaculty\Graph\Graph();
$vertex_one = $graph->createVertex('one');
```

Set the graph on a 'graph' render element, specifying the format plugin ID to
use to render it:

```
$render = [
  '#type' => 'graph',
  '#graph' => $graph,
  '#format' => 'graphapi_png',
];
```

Additional options can be specified:

```
$render = [
  '#type' => 'graph',
  '#graph' => $graph,
  '#format' => 'graphapi_png',
  '#width' => 800,
  '#height' => 800,
  '#options' => [
    // Further options depend on the format plugin.
  ],
];
```

Format plugins define defaults for the options, which can be configured in the
admin UI at /admin/config/system/graphapi. The values given in a render array
always take precedence over plugin definition or configuration.
