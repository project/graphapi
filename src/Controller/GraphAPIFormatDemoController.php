<?php

namespace Drupal\graphapi\Controller;

use Drupal\Core\Controller\ControllerBase;
use Fhaculty\Graph\Graph;

/**
 * Controller for the graph format demo routes.
 */
class GraphAPIFormatDemoController extends ControllerBase {

  /**
   * Route callback for the graph format demo.
   *
   * @param string $format_id
   *   The ID of the format plugin to use.
   */
  public function formatDemo($format_id) {
    $build = [];

    $format = \Drupal::service('plugin.manager.graphapi_graph_format')->createInstance($format_id);

    $build['#title'] = $this->t("Demo for @engine graph format", [
      '@engine' => $format->getLabel(),
    ]);

    $graph = $this->demoGraph();

    $build['graph'] = [
      '#type' => 'graph',
      '#graph' => $graph,
      '#format' => $format_id,
      '#width' => 800,
      '#height' => 800,
    ];

    return $build;
  }

  /**
   * Creates a simple demo graph.
   *
   * @return \Fhaculty\Graph\Graph
   *   The graph.
   */
  public function demoGraph(): Graph {
    $graph = new Graph();

    $tags = $graph->createVertex('tags');
    $tags->setAttribute('title', 'Tags');

    $user = $graph->createVertex('user');
    $user->setAttribute('title', 'Users');

    $article = $graph->createVertex('article');
    $article->setAttribute('title', 'Article');

    $article->createEdgeTo($tags)->setAttribute('title', 'field_tags');
    $article->createEdgeTo($user)->setAttribute('title', 'uid');

    $page = $graph->createVertex('page');
    $page->setAttribute('title', 'Page');

    $page->createEdgeTo($user)->setAttribute('title', 'uid');

    return $graph;
  }

}
