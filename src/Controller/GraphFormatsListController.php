<?php

namespace Drupal\graphapi\Controller;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\graphapi\GraphEngineManager;
use Drupal\graphapi\GraphFormatManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Url;

/**
 * Displays all Graph API formats and engines in a table.
 */
class GraphFormatsListController implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The graph engine manager.
   *
   * @var \Drupal\graphapi\GraphEngineManager
   */
  protected $graphEngineManager;

  /**
   * The graph format manager.
   *
   * @var \Drupal\graphapi\GraphFormatManager
   */
  protected $graphFormatManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.graphapi_graph_engine'),
      $container->get('plugin.manager.graphapi_graph_format'),
    );
  }

  /**
   * Creates a DsfsdfController instance.
   *
   * @param \Drupal\graphapi\GraphEngineManager $graph_engine_manager
   *   The graph engine manager.
   * @param \Drupal\graphapi\GraphFormatManager $graph_format_manager
   *   The graph format manager.
   */
  public function __construct(
    GraphEngineManager $graph_engine_manager,
    GraphFormatManager $graph_format_manager,
  ) {
    $this->graphEngineManager = $graph_engine_manager;
    $this->graphFormatManager = $graph_format_manager;
  }

  /**
   * Route callback to display a table of formats and engines.
   */
  public function build() {
    $build = [];

    $headers = [
      $this->t('Format'),
      $this->t('Engine'),
      $this->t('Description'),
      [
        'data' => $this->t('Operations'),
        'colspan' => 7,
      ],
    ];

    $formats = $this->graphFormatManager->getDefinitions();
    $engines = $this->graphEngineManager->getDefinitions();

    if (empty($formats)) {
      return [
        '#markup' => $this->t('No Graph API formats found.'),
      ];
    }

    uasort($formats, function ($definition_a, $definition_b) {
      return strnatcasecmp($definition_a['label'], $definition_b['label']);
    });

    $rows = [];
    foreach ($formats as $format_id => $format_definition) {
      $row = [
        $format_id,
        $format_definition['engine'],
        $format_definition['label'],
      ];

      $operations = [
        'demo' => [
          'title' => $this->t('Demo'),
          'url' => Url::fromRoute('graphapi.format.demo', ['format_id' => $format_id]),
        ],
        'format_defaults' => [
          'title' => $this->t('Configure format defaults'),
          'url' => Url::fromRoute('graphapi.format.default_options', ['format_id' => $format_id]),
        ],
      ];

      if ($engines[$format_definition['engine']]['configurable']) {
        $operations['configure_engine'] = [
          'title' => $this->t('Configure engine'),
          'url' => Url::fromRoute('graphapi.engine.settings', ['engine_id' => $format_definition['engine']]),
        ];
      }

      $row[] = [
        'data' => [
          '#type' => 'operations',
          '#links' => $operations,
        ],
      ];
      $rows[] = $row;
    }

    $build['table'] = [
      '#type' => 'table',
      '#header' => $headers,
      '#rows' => $rows,
    ];

    return $build;
  }

}
