<?php

namespace Drupal\graphapi\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Controller\FormController;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Fhaculty\Graph\Graph;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormState;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentResolverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller for the graph formats defaults forms.
 *
 * Replaces HtmlEntityFormController for graphapi_format_defaults entities, so
 * that a new entity can be created when the edit form is loaded for one that
 * does not exist.
 */
class GraphFormatDefaultsFormController extends FormController implements ContainerInjectionInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('http_kernel.controller.argument_resolver'),
      $container->get('form_builder'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getFormArgument(RouteMatchInterface $route_match) {
    // The form argument is not in the route: getFormObject() has it hardcoded.
    return '';
  }

  /**
   * {@inheritdoc}
   */
  protected function getFormObject(RouteMatchInterface $route_match, $form_arg) {
    // TODO DI.
    $form_object = \Drupal::service('entity_type.manager')->getFormObject('graphapi_format_defaults', 'edit');

    $storage = \Drupal::service('entity_type.manager')->getStorage('graphapi_format_defaults');

    $format_id = $route_match->getRawParameter('format_id');
    $entity = $storage->load($format_id);

    // If there is no entity for the format ID, create it now.
    if (!$entity) {
      $entity = $storage->create([
        'id' => $format_id,
      ]);
    }

    $form_object->setEntity($entity);

    return $form_object;
  }

}
