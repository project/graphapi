<?php

namespace Drupal\graphapi;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\graphapi\Annotation\GraphEngine;
use Drupal\graphapi\Plugin\GraphEngine\GraphEngineInterface;

/**
 * Manages discovery and instantiation of Graph Engine plugins.
 */
class GraphEngineManager extends DefaultPluginManager {

  /**
   * Constructs a new GraphEngineManager.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The cache backend.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/GraphEngine',
      $namespaces,
      $module_handler,
      GraphEngineInterface::class,
      GraphEngine::class
    );

    $this->alterInfo('graph_engine_info');
    $this->setCacheBackend($cache_backend, 'graph_engine_plugins');
  }

  /**
   * {@inheritdoc}
   */
  public function processDefinition(&$definition, $plugin_id) {
    parent::processDefinition($definition, $plugin_id);

    // Set the 'configurable' property on the definition based on whether the
    // plugin class implements the interface for a form.
    $interfaces = class_implements($definition['class']);
    $definition['configurable'] = isset($interfaces[PluginFormInterface::class]);
  }

}
