<?php

namespace Drupal\graphapi\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Edit form for the Graphapi Format Defaults entity.
 */
class GraphapiFormatDefaultsEditForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $format = \Drupal::service('plugin.manager.graphapi_graph_format')->createInstance($this->entity->id());

    $form['#title'] = $this->t("Edit @format default options", [
      '@format' => $format->getLabel(),
    ]);

    $form = $format->defaultOptionsForm($form, $form_state, $this->entity);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);

    $actions['submit']['#value'] = $this->t("Save defaults");

    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  protected function copyFormValuesToEntity(EntityInterface $entity, array $form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    // Set width and height, which are their own properties on the entity.
    $entity->set('width', $values['width']);
    $entity->set('height', $values['height']);

    unset($values['width']);
    unset($values['height']);

    // Set all other values from the form into the options property.
    $entity->set('options', $values);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $saved = parent::save($form, $form_state);

    \Drupal::service('messenger')->addMessage($this->t("Defaults for the format have been updated."));

    $form_state->setRedirectUrl(Url::FromRoute('graphapi.overview'));

    return $saved;
  }

}
