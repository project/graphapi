<?php

namespace Drupal\graphapi\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure settings for a graph engine.
 */
class EngineSettingsForm extends ConfigFormBase {

  protected $engine;

  /**
   * {@inheritdoc}
   */
  public function getBaseFormId() {
    return 'graphapi_engine_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    $engine_id = \Drupal::routeMatch()->getParameter('engine_id');
    return 'graphapi_engine_settings_' . $engine_id;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $engine_id = '') {
    $this->engine = \Drupal::service('plugin.manager.graphapi_graph_engine')->createInstance($engine_id);

    $form['#title'] = $this->t("Settings for @engine graph engine", [
      '@engine' => $this->engine->getLabel(),
    ]);

    $form = $this->engine->buildConfigurationForm($form, $form_state);

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $this->engine->validateConfigurationForm($form, $form_state);

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->engine->submitConfigurationForm($form, $form_state);

    parent::submitForm($form, $form_state);

    $form_state->setRedirect('graphapi.overview');
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    // Editable config is handled by the engine plugin class. We don't use
    // config directly in this class.
    return [];
  }

}
