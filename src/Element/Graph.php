<?php

namespace Drupal\graphapi\Element;

use Drupal\Core\Render\Element\RenderElementBase;
use Drupal\Core\Template\Attribute;
use Drupal\Core\Render\Element;

/**
 * Provides a render element for a graph.
 *
 * Properties:
 * - #format: The ID of the graph format plugin to use to output the graph.
 * - #graph: The graph object.
 * - #width: The width of the rendered graph. Not all formats may respect this.
 * - #height: The height of the rendered graph. Not all formats may respect
 *   this.
 * - #options: Additional options for the graph. Possible keys and values depend
 *   on the graph engine and format being used.
 * - '#engine': This is set automatically.
 *
 * If values are not provided for width, height, and options, then defaults are
 * used instead. These defaults are initially defined on the format plugins, and
 * can also be changed by admin users. User changes are stored in
 * graphapi_format_defaults config entities.
 *
 * @see \Drupal\graphapi\Entity\GraphapiFormatDefaults
 *
 * @RenderElement("graph")
 */
class Graph extends RenderElementBase {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    return [
      '#theme' => 'graph',
      '#pre_render' => [
        [static::class, 'preRender'],
      ],
    ];
  }

  /**
   * Pre-render callback: allows the engine and format plugins to act.
   *
   * Sets the engine ID, and allows the engine and format plugins to act.
   *
   * @param array $element
   *   The graph element.
   *
   * @return array
   *   The passed-in element.
   */
  public static function preRender($element) {
    $format = \Drupal::service('plugin.manager.graphapi_graph_format')->createInstance($element['#format']);
    $engine = \Drupal::service('plugin.manager.graphapi_graph_engine')->createInstance($format->getEngineId());

    $element['#engine'] = $format->getEngineId();

    // Merge in graph format defaults from plugin definition and config.
    $format->elementAddDefaults($element);

    $element = $engine->preRender($element);
    $element = $format->preRender($element);

    return $element;
  }

}
