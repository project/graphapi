<?php

namespace Drupal\graphapi\Entity\Handler;

use Drupal\Core\Entity\EntityAccessControlHandler;

/**
 * Provides the access handler for the Graphapi Format Defaults entity.
 */
class GraphapiFormatDefaultsAccess extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'administer site configuration');
  }

}
