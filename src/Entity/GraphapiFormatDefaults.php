<?php

namespace Drupal\graphapi\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Stores default options for GraphAPI formats.
 *
 * @ConfigEntityType(
 *   id = "graphapi_format_defaults",
 *   label = @Translation("Graphapi Format Defaults"),
 *   label_collection = @Translation("Graphapi Format Defaults"),
 *   label_singular = @Translation("graphapi format defaults"),
 *   label_plural = @Translation("graphapi format defaultss"),
 *   label_count = @PluralTranslation(
 *     singular = "@count graphapi format defaults",
 *     plural = "@count graphapi format defaultss",
 *   ),
 *   handlers = {
 *     "access" = "Drupal\graphapi\Entity\Handler\GraphapiFormatDefaultsAccess",
 *     "form" = {
 *       "default" = "Drupal\Core\Entity\EntityForm",
 *       "edit" = "Drupal\graphapi\Form\GraphapiFormatDefaultsEditForm",
 *     },
 *   },
 *   admin_permission = "administer graphapi_format_defaults entities",
 *   entity_keys = {
 *     "id" = "id",
 *   },
 *   links = {
 *     "edit-form" = "/admin/config/system/graphapi/format-default/{graphapi_format_defaults}",
 *   },
 *   config_export = {
 *     "id",
 *     "width",
 *     "height",
 *     "options",
 *   },
 * )
 */
class GraphapiFormatDefaults extends ConfigEntityBase implements GraphapiFormatDefaultsInterface {

  /**
   * The entity ID and format ID.
   *
   * @var string
   */
  protected $id = '';

  /**
   * The width of the graph.
   *
   * @var int
   */
  protected $width;

  /**
   * The height of the graph.
   *
   * @var int
   */
  protected $height;

  /**
   * The default options for the format.
   *
   * @var mapping
   */
  protected $options = [];

  /**
   * {@inheritdoc}
   */
  public function getOption(string $name) {
    $default_value = \Drupal::service('plugin.manager.graphapi_graph_format')->createInstance($this->id())->getOptionDefault($name);

    return $this->options[$name] ?? $default_value;
  }

}
