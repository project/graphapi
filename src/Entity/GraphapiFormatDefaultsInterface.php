<?php

namespace Drupal\graphapi\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Interface for Graphapi Format Defaults entities.
 */
interface GraphapiFormatDefaultsInterface extends ConfigEntityInterface {

  /**
   * Gets the value of an option from the entity or from plugin defaults.
   *
   * Format plugins can define default values for their options on their
   * annotation. These will be shown in the format defaults form the first time
   * it is edited.
   *
   * @param string $name
   *   The option name. This is the key in the $options array, and the form
   *   element name in the options form.
   *
   * @return mixed
   *   The option value.
   */
  public function getOption(string $name);

}
