<?php

namespace Drupal\graphapi\Plugin\GraphEngine;

/**
 * Interface for Graph Engine plugins.
 */
interface GraphEngineInterface {

  /**
   * Gets a human readable label.
   *
   * @return string
   *   The label.
   */
  public function getLabel();

  /**
   * Perform pre-rendering on the graph render element.
   *
   * This should be used for attaching JS libraries which are common to all of
   * an engine's formats.
   *
   * @param $element The graph render element.
   *
   * @return
   *   The graph render element.
   */
  public function preRender($element): array;

  /**
   * Preprocess the variables for the graph.html.twig template.
   *
   * @param array $variables
   *   The template variables.
   */
  public function preProcess(&$variables);

}
