<?php

namespace Drupal\graphapi\Plugin\GraphEngine;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for Graph Engine plugins.
 */
abstract class GraphEngineBase extends PluginBase implements GraphEngineInterface {

  /**
   * {@inheritdoc}
   */
  public function getLabel() {
    return $this->getPluginDefinition()['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function preRender($element): array {
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function preProcess(&$variables) {
  }

}
