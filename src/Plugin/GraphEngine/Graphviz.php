<?php

namespace Drupal\graphapi\Plugin\GraphEngine;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\Form\ConfigFormBaseTrait;

/**
 * Defines the graphviz engine.
 *
 * @GraphEngine(
 *   id = "graphviz",
 *   label = @Translation("Graphviz"),
 * )
 */
class Graphviz extends GraphEngineBase implements PluginFormInterface {

  use ConfigFormBaseTrait;

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('graphapi.graphviz');

    $form['graphviz_path'] = [
      '#type' => 'textfield',
      '#title' => t('Path to Graphviz dot executable'),
      '#default_value' => $config->get('graphviz_path'),
      '#required' => TRUE
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->config('graphapi.graphviz')
      ->set('graphviz_path', $form_state->getValues()['graphviz_path'])
      ->save();
  }

  protected function configFactory() {
    // TODO: inject this instead.
    return \Drupal::service('config.factory');
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['graphapi.graphviz'];
  }

}
