<?php

namespace Drupal\graphapi\Plugin\views\style;

use Drupal\Core\Form\FormStateInterface;
use Drupal\graphapi\GraphFormatManager;
use Drupal\graphapi\Plugin\GraphFormat\GraphFormatInterface;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\Plugin\views\style\Mapping;
use Drupal\views\ViewExecutable;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Style plugin to render a view as a graph.
 *
 * @ViewsStyle(
 *   id = "graphapi_graph",
 *   title = @Translation("Graph"),
 *   help = @Translation("Displays data in a graph."),
 *   theme = "graphapi_view_graph",
 *   display_types = {"normal"}
 * )
 */
class Graph extends Mapping {

  /**
   * {@inheritdoc}
   */
  protected $usesFields = TRUE;

  /**
   * {@inheritdoc}
   */
  protected $usesRowPlugin = FALSE;

  /**
   * The Graph format manager.
   *
   * @var \Drupal\graphapi\GraphFormatManager
   */
  protected $graphFormatManager;

  /**
   * Creates a Graph instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\graphapi\GraphFormatManager $graph_format_manager
   *   The Graph format manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    GraphFormatManager $graph_format_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->graphFormatManager = $graph_format_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.graphapi_graph_format')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['format_id'] = ['default' => NULL];
    $options['directed'] = ['default' => TRUE];
    $options['width'] = ['default' => GraphFormatInterface::COMMON_INITIAL_DEFAULTS['width']];
    $options['height'] = ['default' => GraphFormatInterface::COMMON_INITIAL_DEFAULTS['height']];
    $options['warn_empty_id'] = ['default' => TRUE];
    $options['warn_multiple_id'] = ['default' => TRUE];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  protected function defineMapping() {
    return [
      'from_id' => [
        '#title' => $this->t('From ID'),
        '#description' => $this->t('The ID of the graph vertext an edge starts from. If this is a multi-valued entity refefence, use the ID field on the relationship to ensure multiple result rows and thus multiple edges.'),
        '#required' => TRUE,
      ],
      'from_label' => [
        '#title' => $this->t('From vertex label'),
        '#description' => $this->t('The label to show on the from vertex. If omitted, the ID is used.'),
      ],
      'to_id' => [
        '#title' => $this->t('To ID'),
        '#description' => $this->t('The ID of the graph vertext an edge goes to.  If this is a multi-valued entity refefence, use the ID field on the relationship to ensure multiple result rows and thus multiple edges.'),
        '#required' => TRUE,
      ],
      'to_label' => [
        '#title' => $this->t('To vertex label'),
        '#description' => $this->t('The label to show on the to vertex. If omitted, the ID is used.'),
      ],
      'edge_label' => [
        '#title' => $this->t('Edge label'),
        '#description' => $this->t('The label to show on the edge. This is not supported by all engines.'),
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    $graph_formats = $this->graphFormatManager->getDefinitions();
    $options = [];
    foreach ($graph_formats as $format_id => $format_definition) {
      $options[$format_id] = $format_definition['label'];
    }
    natcasesort($options);

    $form['format_id'] = [
      '#type' => 'radios',
      '#title' => $this->t('Graph format'),
      '#required' => TRUE,
      '#options' => $options,
      '#default_value' => $this->options['format_id'],
    ];

    parent::buildOptionsForm($form, $form_state);

    $edge_label_supporting_formats = $this->graphFormatManager->getFormatsSupportingAttribute('edge', 'title');
    $form['mapping']['edge_label']['#states'] = [
      // Show the settings if 'bar' has been selected for 'foo'.
      'enabled' => array_map(function ($format_id) {
          return [
            ':input[name="style_options[format_id]"]' => ['value' => $format_id]
          ];
        }, $edge_label_supporting_formats),
    ];

    $form['directed'] = [
      '#type' => 'radios',
      '#title' => $this->t('Graph type'),
      '#required' => TRUE,
      '#options' => [
        FALSE => $this->t("Undirected graph"),
        TRUE => $this->t("Directed graph"),
      ],
      '#default_value' => $this->options['directed'],
    ];

    $form['width'] = [
      '#type' => 'number',
      '#title' => $this->t('Graph width'),
      '#default_value' => $this->options['width'],
    ];

    $form['height'] = [
      '#type' => 'number',
      '#title' => $this->t('Graph height'),
      '#default_value' => $this->options['height'],
    ];

    $form['warn_empty_id'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Warn for empty IDs'),
      '#description' => $this->t('Show a warning when a row is skipped because a vertex ID field has an empty value.'),
      '#default_value' => $this->options['warn_empty_id'],
    ];

    $form['warn_multiple_id'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Warn for multiple IDs'),
      '#description' => $this->t("Show a warning when a row's vertex ID field has a multiple value."),
      '#default_value' => $this->options['warn_multiple_id'],
    ];
  }

}
