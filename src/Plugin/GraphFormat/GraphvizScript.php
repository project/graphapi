<?php

namespace Drupal\graphapi\Plugin\GraphFormat;

use Graphp\GraphViz\GraphViz;

/**
 * Defines the graphviz script file format.
 *
 * @GraphFormat(
 *   id = "graphapi_graphviz_script",
 *   label = @Translation("Graph API graphviz script file"),
 *   engine = "graphviz",
 * )
 */
class GraphvizScript extends GraphFormatBase {

  /**
   * {@inheritdoc}
   */
  public function preProcess(&$variables) {
    $graph = $variables['graph'];
    $this->prepareGraphAttributes($graph);
    $viz = new GraphViz($graph);
    $variables['content'] = '<xmp>' . $viz->createScript($graph) . '</xmp>';
  }

}
