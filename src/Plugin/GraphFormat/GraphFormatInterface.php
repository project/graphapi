<?php

namespace Drupal\graphapi\Plugin\GraphFormat;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Interface for Graph Format plugins.
 *
 * Graph formats may provide different options, which are defined in the
 * 'default_options' annotation.
 *
 * Graph formats should indicate attributes they support in the
 * 'supported_attributes' annotation. All graph formats must support the 'title'
 * attribute for vertices, so this does not need to be specified.
 */
interface GraphFormatInterface {

  /**
   * Initial default values for format engines.
   */
  public const COMMON_INITIAL_DEFAULTS = [
    'height' => '800',
    'width' => '800',
  ];

  /**
   * Gets a human readable label.
   *
   * @return string
   *   The label.
   */
  public function getLabel();

  /**
   * Gets the plugin ID of the engine for this format.
   *
   * @return string
   *   The engine plugin ID.
   */
  public function getEngineId(): string;

  /**
   * Provide the form for the format's default options.
   *
   * Implementations should call the parent method to get standard options.
   *
   * Default values form form elements can be obtained from the entity with
   * GraphapiFormatDefaultsInterface::getOption().
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The graphapi_format_defaults entity.
   *
   * @return array
   *   The form array.
   */
  public function defaultOptionsForm(array $form, FormStateInterface $form_state, EntityInterface $entity);

  /**
   * Perform pre-rendering on the graph render element.
   *
   * This should be used for attaching JS libraries which are specific to this
   * format.
   *
   * @param $element The graph render element.
   *
   * @return
   *   The graph render element.
   */
  public function preRender($element): array;

  /**
   * Preprocess the variables for the graph.html.twig template.
   *
   * This should be used to create the HTML markup for the graph, which should
   * be set in $variables['content'].
   *
   * @param array $variables
   *   The template variables.
   */
  public function preProcess(&$variables);

}
