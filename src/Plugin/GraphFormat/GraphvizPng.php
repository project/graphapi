<?php

namespace Drupal\graphapi\Plugin\GraphFormat;

use Graphp\GraphViz\GraphViz;

/**
 * Defines the graphviz PNG format.
 *
 * @GraphFormat(
 *   id = "graphapi_png",
 *   label = @Translation("Graph API PNG by graphviz"),
 *   engine = "graphviz",
 * )
 */
class GraphvizPng extends GraphFormatBase {

  /**
   * {@inheritdoc}
   */
  public function preProcess(&$variables) {
    $graph = $variables['graph'];
    $this->prepareGraphAttributes($graph);
    $viz = new GraphViz($graph);
    $executable = \Drupal::config('graphapi.graphviz')->get('graphviz_path');
    $viz->setExecutable($executable);
    $viz->setFormat('png');
    $variables['content'] = '<img src="' . $viz->createImageSrc($graph) . '" />';
  }

}
