<?php

namespace Drupal\graphapi\Plugin\GraphFormat;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Fhaculty\Graph\Graph;
use Graphp\GraphViz\GraphViz;

/**
 * Base class for Graph Format plugins.
 */
abstract class GraphFormatBase extends PluginBase implements GraphFormatInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getLabel() {
    return $this->getPluginDefinition()['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function getEngineId(): string {
    return $this->getPluginDefinition()['engine'];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultOptionsForm(array $form, FormStateInterface $form_state, EntityInterface $entity) {
    $form['width'] = [
      '#type' => 'number',
      '#title' => $this->t('Width'),
      '#default_value' => $entity->get('width'),
    ];

    $form['height'] = [
      '#type' => 'number',
      '#title' => $this->t('Height'),
      '#default_value' => $entity->get('height'),
    ];

    return $form;
  }

  /**
   * Get the initial value for a format option.
   *
   * This gets the initial value defined on the format plugin's annotation.
   * For common values, the initial values are defined on the Graph render
   * element plugin.
   *
   * @param string $format_id
   *   The ID of the format to get a value for.
   * @param string $option_name
   *   The name of the option. This is a key in the "default_options" array
   *   on the plugin annotation.
   *
   * @return mixed
   *   The initial value.
   *
   * @throws \InvalidArgumentException
   *   Throws an exception if $option_name is neither defined as a common option
   *   nor on the format plugin.
   */
  public function getOptionDefault($option_name) {
    if (isset(self::COMMON_INITIAL_DEFAULTS[$option_name])) {
      return self::COMMON_INITIAL_DEFAULTS[$option_name];
    }
    else {
      $definition = $this->getPluginDefinition();

      if (!isset($definition['default_options'][$option_name])) {
        throw new \InvalidArgumentException(sprintf("Invalid option '%s' for format plugin '%s'.", $option_name, $this->getPluginId()));
      }

      return $definition['default_options'][$option_name];
    }
  }

  /**
   * Add defaults from config and plugin to the render element.
   *
   * @param array $element
   *   The render element, passed by reference.
   */
  public function elementAddDefaults(&$element) {
    $element += ['#options' => []];

    // Add defaults from the config entity, if one exists.
    $default_options_entity = \Drupal::service('entity_type.manager')->getStorage('graphapi_format_defaults')->load($element['#format']);
    if ($default_options_entity) {
      $element['#width'] = $element['#width'] ?? $default_options_entity->get('width');
      $element['#height'] = $element['#height'] ?? $default_options_entity->get('height');

      $element['#options'] += $default_options_entity->get('options');
    }

    // Add defaults from the plugin definition.
    $element['#width'] = $element['#width'] ?? static::COMMON_INITIAL_DEFAULTS['width'];
    $element['#height'] = $element['#height'] ?? static::COMMON_INITIAL_DEFAULTS['height'];

    $element['#options'] += $this->getPluginDefinition()['default_options'];
  }

  /**
   * {@inheritdoc}
   */
  public function preRender($element): array {
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function preProcess(&$variables) {
  }

  /**
   * Prepares the graph's attributes.
   *
   * @param \Fhaculty\Graph\Graph $graph
   *   The graph.
   */
  protected function prepareGraphAttributes(Graph $graph) {
    // http://www.graphviz.org/doc/info/shapes.html#html
    $html = array(
      'label',
      'headtail',
      'taillabel'
    );
    $mapping = array(
      'title' => 'label',
      'uri' => 'URL',
        // TODO 'content' => should be mapped to a note vertex
    );
    foreach ($graph->getVertices() as $vertex) {
      $attributes = $vertex->getAttributeBag()->getAttributes();
      foreach ($attributes as $from => $value) {
        if (isset($mapping[$from])) {
          $attributes[$from] = NULL;
          $attributes[$mapping[$from]] = $value;
        }
      }
      foreach ($html as $attr) {
        if (isset($attributes[$attr])) {
          $value = $attributes[$attr];
          $insecure_value = html_entity_decode($value);
          if (preg_match("/^\<\<.*\>\>$/", $insecure_value)) {
            $value = GraphViz::raw($insecure_value);
          }
          $attributes[$attr] = $value;
        }
      }
      // Note that this won't remove attributes we removed from $attributes, as
      // setAttributes() merges with what is already there.
      $vertex->getAttributeBag()->setAttributes($attributes);
    }
    foreach ($graph->getEdges() as $edge) {
      $attributes = $edge->getAttributeBag()->getAttributes();
      foreach ($attributes as $from => $value) {
        if (isset($mapping[$from])) {
          $attributes[$from] = NULL;
          $attributes[$mapping[$from]] = $value;
        }
      }
      $edge->getAttributeBag()->setAttributes($attributes);
    }
  }

}
