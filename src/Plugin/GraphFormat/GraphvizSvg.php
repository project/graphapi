<?php

namespace Drupal\graphapi\Plugin\GraphFormat;

use Graphp\GraphViz\GraphViz;

/**
 * Defines the graphviz SVG format.
 *
 * @GraphFormat(
 *   id = "graphapi_svg",
 *   label = @Translation("Graph API SVG by graphviz"),
 *   engine = "graphviz",
 * )
 */
class GraphvizSvg extends GraphFormatBase {

  /**
   * {@inheritdoc}
   */
  public function preProcess(&$variables) {
    $graph = $variables['graph'];
    $this->prepareGraphAttributes($graph);
    $viz = new GraphViz($graph);
    $executable = \Drupal::config('graphapi.graphviz')->get('graphviz_path');
    $viz->setExecutable($executable);
    $viz->setFormat('svg');
    $variables['content'] = '<img src="' . $viz->createImageSrc($graph) . '" />';
  }

}
