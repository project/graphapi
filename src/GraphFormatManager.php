<?php

namespace Drupal\graphapi;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\graphapi\Annotation\GraphFormat;
use Drupal\graphapi\Plugin\GraphFormat\GraphFormatInterface;

/**
 * Manages discovery and instantiation of Graph Format plugins.
 */
class GraphFormatManager extends DefaultPluginManager {

  /**
   * Constructs a new GraphFormatManager.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The cache backend.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/GraphFormat',
      $namespaces,
      $module_handler,
      GraphFormatInterface::class,
      GraphFormat::class
    );

    $this->alterInfo('graph_format_info');
    $this->setCacheBackend($cache_backend, 'graph_format_plugins');
  }

  /**
   * Determines whether a format supports a graph attribute.
   *
   * @param string $format_id
   *   The plugin ID of the format.
   * @param string $location
   *   The location of the attribute to check. One of 'graph', 'vertex', 'edge'.
   * @param string $attribute
   *   The name of the attribute to check.
   *
   * @return bool
   *   TRUE if the graph format supports the attribute, FALSE if it does not.
   */
  public function formatSupportsAttribute(string $format_id, string $location, string $attribute): bool {
    return in_array($attribute, $this->getDefinition($format_id)['supported_attributes'][$location]);
  }

  /**
   * Gets a list of format ID which support a given attribute.
   *
   * @param string $location
   *   The location of the attribute to get formats for. One of 'graph',
   *   'vertex', 'edge'.
   * @param string $attribute
   *   The name of the attribute to get formats for.
   *
   * @return string[]
   *   An array of format plugin IDs.
   */
  public function getFormatsSupportingAttribute(string $location, string $attribute): array {
    $supporting_formats = [];

    foreach ($this->getDefinitions() as $format_id => $format_definition) {
      if ($this->formatSupportsAttribute($format_id, $location, $attribute)) {
        $supporting_formats[] = $format_id;
      }
    }

    return $supporting_formats;
  }

  /**
   * {@inheritdoc}
   */
  public function processDefinition(&$definition, $plugin_id) {
    // Ensure that the array of supported attributes always has the keys for
    // the different locations.
    $definition['supported_attributes'] += [
      'graph' => [],
      'vertex' => [],
      'edge' => [],
    ];
  }

}
