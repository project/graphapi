<?php

namespace Drupal\graphapi\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines the Graph Engine plugin annotation object.
 *
 * Plugin namespace: GraphEngine.
 *
 * @Annotation
 */
class GraphEngine extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id = '';

  /**
   * The human-readable name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $label = '';

  /**
   * Whether this engine has configuration.
   *
   * This is set automatically based on whether the plugin class implements
   * \Drupal\Core\Plugin\PluginFormInterface.
   *
   * @var bool
   */
  public $configurable = FALSE;

}
