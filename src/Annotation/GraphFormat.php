<?php

namespace Drupal\graphapi\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines the Graph Format plugin annotation object.
 *
 * Plugin namespace: GraphFormat.
 *
 * @Annotation
 */
class GraphFormat extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id = '';

  /**
   * The human-readable name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $label = '';

  /**
   * The engine plugin ID.
   *
   * @var string
   */
  public $engine = '';

  /**
   * List of attributes the format supports.
   *
   * @var array
   *   An array whose keys are any of 'graph', 'vertex', 'edge'. Each value is
   *   an array of attribute names which the format supports. The 'title' a
   *   attribute for vertices does not need to be included here, as all formats
   *   must support it.
   */
  public $supported_attributes = [];

  /**
   * Initial values for format default options.
   *
   * Values for common options are defined on the format plugin interface.
   *
   * @var array
   */
  public $default_options = [];

}
