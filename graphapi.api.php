<?php

/**
 * @defgroup graphapi Graph API module integrations.
 *
 * Module integrations with the graphapi module.
 *
 * Each engine can provide multiple formats.
 * Ie a textual and a visual representation of it's structure
 */

/**
 * Perform alterations on Graph Engine definitions.
 *
 * @param array $info
 *   Array of information on Graph Engine plugins.
 */
function hook_graph_engine_info_alter(array &$info) {
  // Change the class of the 'foo' plugin.
  $info['foo']['class'] = SomeOtherClass::class;
}

/**
 * Perform alterations on Graph Format definitions.
 *
 * @param array $info
 *   Array of information on Graph Format plugins.
 */
function hook_graph_format_info_alter(array &$info) {
  // Change the class of the 'foo' plugin.
  $info['foo']['class'] = SomeOtherClass::class;
}

/**
 * Declares settings that should be exportable in Views.
 *
 * TODO: rewrite this views specific structure to normal.
 * - that is remove default
 *
 * The settings declared here will be included in the option_definition()
 * method, as implemented in the Graph API style plugin.
 *
 * @return array
 *   A views_plugin_style::option_definition() compatible structure.
 *
 * @see views_plugin_style::option_definition()
 */
function hook_graphapi_default_settings() {
  // Settings used by the first graph engine.
  $settings['my_first_engine']['contains'] = array(
    'setting 1' => array('default' => NULL),
    'setting 2' => array('default' => 1),
    'setting 3' => array('default' => t('Some string')),
  );
  // Settings used by the second graph engine.
  $settings['my_second_engine']['contains']['setting 1']['default'] = 1;
  return $settings;
}

/**
 * Example function: creates a graph of user logins by day.
 */
function graphapi_API_user_last_login_by_day($n = 40) {
  $query = \Drupal::database()->select('users');
  $query->addField('users', 'name');
  $query->addField('users', 'uid');
  $query->addField('users', 'created');
  $query->condition('uid', 0, '>');
  $query->orderBy('created', 'DESC');
  $query->range(0, $n);
  $result = $query->execute();
  $g = graphapi_new_graph();
  $now = time();
  $days = array();
  foreach ($result as $user) {
    $uid = $user->uid;
    $user_id = 'user_' . $uid;

    $day = intval(($now - $user->created) / (24 * 60 * 60));
    $day_id = 'data_' . $day;
    graphapi_set_node_title($g, $user_id, l($user->name, "user/" . $uid));
    graphapi_set_node_title($g, $day_id, "Day " . $day);
    $link = graphapi_add_link($g, $user_id, $day_id);
    graphapi_set_link_data($link, array('color' => '#F0F'));
  }
  $options = array(
    'width' => 400,
    'height' => 400,
    'item-width' => 50,
    'engine' => 'graph_phyz'
  );
  return theme('graphapi_dispatch', array('graph' => $g, 'config' => $options));
}
