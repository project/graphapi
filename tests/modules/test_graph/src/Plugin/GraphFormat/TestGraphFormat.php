<?php

namespace Drupal\test_graph\Plugin\GraphFormat;

use Drupal\graphapi\Plugin\GraphFormat\GraphFormatBase;

/**
 * Test graph format with options.
 *
 * This outputs width, height, and all options as plain text when rendered.
 *
 * @GraphFormat(
 *   id = "test_graph_format",
 *   label = @Translation("Test Graph Format"),
 *   engine = "test_graph_engine",
 *   default_options = {
 *     "alpha" = "alpha-default",
 *     "beta" = "beta-default",
 *   },
 * )
 */
class TestGraphFormat extends GraphFormatBase {

  /**
   * {@inheritdoc}
   */
  public function preProcess(&$variables) {
    $pieces[] = "width=" . $variables['width'];
    $pieces[] = "height=" . $variables['height'];

    foreach ($variables['options'] as $option_name => $value) {
      $pieces[] = "$option_name=$value";
    }
    $variables['content'] = implode("\n", $pieces);
  }

}
