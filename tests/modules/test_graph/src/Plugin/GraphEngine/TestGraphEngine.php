<?php

namespace Drupal\test_graph\Plugin\GraphEngine;

use Drupal\graphapi\Plugin\GraphEngine\GraphEngineBase;

/**
 * Test graph engine.
 *
 * @GraphEngine(
 *   id = "test_graph_engine",
 *   label = @Translation("Test Graph Engine"),
 * )
 */
class TestGraphEngine extends GraphEngineBase {

}
