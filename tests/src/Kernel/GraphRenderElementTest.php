<?php

namespace Drupal\Tests\graphapi\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Tests the graph render element.
 *
 * @group graphapi
 */
class GraphRenderElementTest extends KernelTestBase {

  /**
   * The modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'system',
    'user',
    'graphapi',
    'test_graph',
  ];

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['graphapi']);
    $this->installEntitySchema('graphapi_format_defaults');

    $this->entityTypeManager = $this->container->get('entity_type.manager');
  }

  /**
   * Tests the default values for rendering a graph.
   */
  public function testFormatDefaults() {
    // No options in render element: default options come from format plugin
    // definition; width and height come from format plugin interface.
    $build = [
      '#type' => 'graph',
      '#format' => 'test_graph_format',
      '#graph' => '',
    ];
    $result = $this->render($build);
    $this->assertStringContainsString('width=800', $result);
    $this->assertStringContainsString('height=800', $result);
    $this->assertStringContainsString('alpha=alpha-default', $result);
    $this->assertStringContainsString('beta=beta-default', $result);

    // Options in render element override.
    $build = [
      '#type' => 'graph',
      '#format' => 'test_graph_format',
      '#graph' => '',
      '#width' => 50,
      '#height' => 50,
      '#options' => [
        'alpha' => 'alpha-render',
        'beta' => 'beta-render',
      ],
    ];
    $result = $this->render($build);
    $this->assertStringContainsString('width=50', $result);
    $this->assertStringContainsString('height=50', $result);
    $this->assertStringContainsString('alpha=alpha-render', $result);
    $this->assertStringContainsString('beta=beta-render', $result);

    // Add a config format defaults entity.
    $storage = $this->entityTypeManager->getStorage('graphapi_format_defaults');
    $config = $storage->create([
      'id' => 'test_graph_format',
      'width' => 200,
      'height' => 300,
      'options' => [
        'alpha' => 'alpha-config',
        'beta' => 'beta-config',
      ],
    ])->save();

    // No options in render element: default options come from config entity.
    $build = [
      '#type' => 'graph',
      '#format' => 'test_graph_format',
      '#graph' => '',
    ];
    $result = $this->render($build);
    $this->assertStringContainsString('width=200', $result);
    $this->assertStringContainsString('height=300', $result);
    $this->assertStringContainsString('alpha=alpha-config', $result);
    $this->assertStringContainsString('beta=beta-config', $result);

    // Options in render element override config entity.
    $build = [
      '#type' => 'graph',
      '#format' => 'test_graph_format',
      '#graph' => '',
      '#width' => 50,
      '#height' => 50,
      '#options' => [
        'alpha' => 'alpha-render',
        'beta' => 'beta-render',
      ],
    ];
    $result = $this->render($build);
    $this->assertStringContainsString('width=50', $result);
    $this->assertStringContainsString('height=50', $result);
    $this->assertStringContainsString('alpha=alpha-render', $result);
    $this->assertStringContainsString('beta=beta-render', $result);
  }

}
